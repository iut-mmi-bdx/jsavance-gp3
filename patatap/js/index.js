/* 
Récupère le nom de notre utilisateur,
& on l'affiche dans le DOM
*/
const getName = () => {
    const name = prompt(`Comment t'appelles-tu ?`)
    const span = document.getElementById('name')
    span.textContent = `Bonjour ${name}`
}


/*
Écoute notre DOM à chque touche du clavier appuyée,
& on affiche la valeur de la touche si c'est une lettre
*/
window.addEventListener(
    'keyup',
    (event) => {

        const alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
        const isLetter = alphabet.includes(event.key);
        if (isLetter) {
            playSound(event.key)
            animationLetter.push(event.key)
        }
    }
)

const playSound = (key) => {
    const url = `sounds/${key}.mp3`
    const audio = new Audio(url)
    audio.play()

    audio.addEventListener(
        'ended',
        () => {
            const index = animationLetter.indexOf(key)
            animationLetter.splice(index, 1)
        }
    )
}

/*

Écrire une fonction de pré-chargement 
de sons

- OK lancée au démarrage de notre site
- elle précharge les sons : canplaythrough => évènement
- quand un son est chargé elle l'affiche dans le HTML
- affichage du HTML : x / 26
- quand tous les sons ont fini de charger, 
on demande le prénom à notre utilisateur

*/

const loader = () => {
    let numberOfSounds = 0
    const number = document.getElementById('number')

    // 1 - Précharger tous les sons
    const alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    for (let i = 0; i < alphabet.length; i++) {
        const soundUrl = `sounds/${alphabet[i]}.mp3`
        const audio = new Audio(soundUrl)

        audio.addEventListener(
            'canplaythrough',
            () => {
                // 2 - Quand un son est chargé, on l'affiche dans le HTML
                numberOfSounds++
                number.textContent = numberOfSounds

                // 3 - Quand tous les sons sont chargés 
                // on demande le prénom de l'utilisateur
                if ( numberOfSounds === 26 ) {
                    setTimeout( () => {
                        getName()
                        const loader = document.getElementById('loader')
                        loader.classList.add('hide')
                    }, 1000)
                }
            }
        )

        /*
        qui.addEventListener(
            'quoi' : string
            () => {
                // ce que je fais à chaque fois que l'évènement se déclenche
                // console.log('chargé')
            }
        )
        */
    }
}

loader()

const width = window.innerWidth
const height = window.innerHeight
let animationLetter = []
let colors = [
    [255, 0, 0], 
    [0, 255, 0]
]

let colorsEva = [
    'string', 
    'string'
]

function setup() {
    const canvas = createCanvas(width, height)
    canvas.parent('canvasContainer')
}

function draw(){
    clear()
    background('rgba(0, 0, 0, 0)')
    if (animationLetter.includes('a')) shapeA()
    if (animationLetter.includes('b')) shapeB()
}

const shapeA = () => {
    const r = colors[0][0]
    const g = colors[0][1]
    const b = colors[0][2]
    fill(r, g, b)
    ellipse(200, 200, 100)
}

const shapeB = () => {
    const r = colors[1][0]
    const g = colors[1][1]
    const b = colors[1][2]
    fill(r, g, b)
    ellipse(width / 2, height / 2, 100)
}


/*
Écrire une fonction pour afficher et cacher notre liste d'images
*/

const button = document.getElementById('colorsButton')
const list = document.getElementById('colorsContainer')

button.addEventListener(
    'click',
    () => {
        list.classList.toggle('hide')
    }
)

/*

1 - OK Qd j'ai cliqué sur une touche on avait une couleur par defaut
2 - OK On clic sur une image du burger
3 - OK Faire changer les couleurs : 
générer une palette de couleur à partir de l'image cliquée
4 - Changer les couleurs de nos formes

*/

const images = document.querySelectorAll('.patatap__image')

for (let i = 0; i < images.length; i++){

    const image = images[i]
    
    image.addEventListener(
        'click',
        () => {
            const src = image.getAttribute('src')
            const vibrant = new Vibrant(src)
            vibrant.getPalette( (error, palette) => {
                colors = []
                if (palette.DarkMuted) colors.push(palette.DarkMuted._rgb)
                if (palette.DarkVibrant) colors.push(palette.DarkVibrant._rgb)
                if (palette.LightMuted) colors.push(palette.LightMuted._rgb)
                if (palette.LightVibrant) colors.push(palette.LightVibrant._rgb)
                if (palette.Muted) colors.push(palette.Muted._rgb)
                if (palette.Vibrant) colors.push(palette.Vibrant._rgb)
                button.click()
            })
        }
    )
}