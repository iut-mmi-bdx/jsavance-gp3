/*
ES5
Rappel variables, fonctions
*/
var nom = 'valeur' // string
var bool = false // boolean
var int = 1 // integer
var float = 1.222 // float
var array = [1, '2', 3] // tableau
var object = {
    nom: 'valeur',
    prenom: 'valeur'
} // objet

function showName(name) {
    // programme
    console.log('Mon nom est :', name)
}


/*
ES6
Comment on écrit les variables et les fonctions avec la nouvelle notation
*/

// 1 - variables
// si la variable est modifiable, on utilise le mot-clé "let"
let age = 27 // variable modifiable
age = 28

const name = 'Charline'// variable non modifiable
//name = 'Charles' // ce code produit une erreur, il n'est pas possible de modifier une variable déclarée avec "const"


// 2 - fonctions
const myFunction = (params) => {
    console.log('mes paramètres sont', params)
}


/* 
Écrire une fonction qui prend en paramètre un tableau. 
Le tableau est composé en premier d’un nombre, votre âge, 
puis un objet avec propriétés nom / prénom
La fonction fonction retourne « Bonjour je m’appelle x y et j’ai x ans »
Console.log le résultat de la fonction
*/

const getSentence = (array) => {
    const age = array[0]
    const firstname = array[1].firstname
    const lastname = array[1].lastname
    console.log(`Bonjour je m'apelle ${firstname} ${lastname} et j'ai ${age} ans`)
}
const myAge = 27
const myObject = {
    lastname: 'Laporte',
    firstname: 'Charline'
}
const myArray = [myAge, myObject]
getSentence(myArray)